#!pyobjects

Pkg.installed("php")
Pkg.installed("imagemagick")
Pkg.installed("php-xml")
Pkg.installed("php-sqlite3")
Pkg.installed("php-curl")

Service.running(
    "nginx-factorio",
    name="nginx",
    reload=True,
)

File.managed(
    "/etc/nginx/sites-enabled/factorio.agmlego.com",
    source="salt://nginx.conf",
    listen_in=[
        Service("nginx-factorio"),
    ],
)

with File.recurse(
    "/usr/share/nginx/factorio.agmlego.com",
    source="salt://_artifacts/dokuwiki",
    include_empty=True,
):
    File.directory("/usr/share/nginx/factorio.agmlego.com/.well-known")
    for dname in (
        "/usr/share/nginx/factorio.agmlego.com/data",
        "/usr/share/nginx/factorio.agmlego.com/lib/plugins",
        "/usr/share/nginx/factorio.agmlego.com/lib/tpl",
        "/usr/share/nginx/factorio.agmlego.com/conf",
    ):
        File.directory(
            dname,
            user='www-data',
            group='www-data',
            recurse=True,
        )
